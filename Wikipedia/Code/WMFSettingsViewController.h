#import <UIKit/UIKit.h>
#import "StreetHawkCore_Pointzi.h"

@class MWKDataStore, WMFArticleDataStore;

@interface WMFSettingsViewController : StreetHawkBaseViewController

+ (instancetype)settingsViewControllerWithDataStore:(MWKDataStore *)store previewStore:(WMFArticleDataStore *)previewStore;

@property (nonatomic, strong, readonly) MWKDataStore *dataStore;
@property (nonatomic, strong, readonly) WMFArticleDataStore *previewStore;

@end
