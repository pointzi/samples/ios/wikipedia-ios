#!/bin/sh -x
echo "Build started on $(date)"
set -e

rm -Rf ./build/outputs/*

echo "Record git branch and commit hash ========================================"

GIT_VERSION=$(git log -1 --format="%h")
if [ -z "$CI_COMMIT_REF_NAME" ]; then
    GIT_BRANCH=$(git symbolic-ref --short -q HEAD)
else
    GIT_BRANCH=$CI_COMMIT_REF_NAME
fi
BUILD_TIME=$(date)

/usr/libexec/PlistBuddy -c "Set :CFBundleVersion '${GIT_BRANCH}-${GIT_VERSION}-${BUILD_TIME}'" "./Wikipedia/Wikipedia-Info.plist"


# ------------------- build Wikipedia ------------------------

echo "Run XCode to build App ==================================================="
printenv
export GEM_HOME=~/.gem
export FL_UNLOCK_KEYCHAIN_PASSWORD=$BUILD_PASSWORD
export FL_UNLOCK_KEYCHAIN_SET_DEFAULT="/Users/hawk/Library/Keychains/login.keychain-db"

gem install fastlane
bundle update

pod install
fastlane install_plugins
fastlane carthage_update
fastlane beta_release
